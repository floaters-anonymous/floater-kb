[!img:](https://gitlab.com/uploads/-/system/project/avatar/41185754/onewheel_xr.jpg)
# floaters-anonymous

mirror:
((https://github.com/floaters-anonymous/floater-kb))

### Reference:

* [1wheel on Gitbook.io](https://app.gitbook.com/invite/CAI2PvcEBO6EQLwSDjOW/N7ftWtxnJP35ZuZ7ps1O)
* [esk8_diy-onewheel-hoverboard](https://forum.esk8.news/t/diy-onewheel-hoverboard/18862)


## License
<a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png" /></a><br />This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">Creative Commons Attribution-ShareAlike 4.0 International License</a>.
